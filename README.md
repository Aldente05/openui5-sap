# OpenUI 5 SAP #

This website template for learn openui5 in hardway, or you can modify this template for standard, or entreprise use.

### What is this repository for? ###

* Prototype Pattern Javascript
* RequireJS Module loader
* [RequireJS](http://requirejs.org/)
* [PrototypeJS](http://prototypejs.org/)
* [OpenUI 5](http://openui5.org/)

### How do I get set up? ###

* Install Nginx
* Configuration Nginx for your environment
* Start Nginx Service
* open http://localhost
* ---

### Contribution guidelines ###

* for Contribution
* Code review
* Other guidelines
--Please Mail to dex.weldes@gmail.com

I designed this template without deleting any library,
so you can learn which library you use.